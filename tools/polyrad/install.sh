#!/bin/bash

# This program is to set up a docker image for the polyRAD 2.0.0.
# It assumes a Debian  12 base image has been used for the container.

# Update image
apt-get update
apt-get -y upgrade

# Install Dependencies via apt
apt install -y \
    r-cran-biocmanager \
    r-bioc-pcamethods \
    r-bioc-variantannotation

# Install polyRAD
Rscript -e "install.packages('polyRAD')"

# Install polyRAD tutorial
Rscript -e "install.packages('polyRADtutorials', repos='https://lvclark.r-universe.dev')"

# Clean up - Remove any packages / files needed only to build the image (e.g. build-essential) and clean the apt cache files.
apt-get clean
