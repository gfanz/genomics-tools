## Tool Name
polyRAD
## Version
2.0.0
## Citation
https://doi.org/10.1534/g3.118.200913
## How to use GFANZ Docker Containers
https://gitlab.com/gfanz/genomics-tools/-/wikis/Using-GFANZ-Genomics-Tools
## Documentation
https://github.com/lvclark/polyRAD/tree/main/man
## Source Code
https://github.com/lvclark/polyRAD
## License
GPL-2.0-or-later
