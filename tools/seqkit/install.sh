#!/bin/bash

# This program is to set up a docker image for SeqKit v2.3.1.
# It assumes a Debian 12 base image has been used for the container.

# Update image
apt-get update
apt-get -y upgrade

# install seqkit
apt-get install -y seqkit

# Clean up - Remove any packages / files needed only to build the image (e.g. build-essential) and clean the apt cache files.
apt-get clean
