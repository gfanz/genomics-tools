#!/bin/bash

# Try helloworld.sh  and see if it is returns helloworld.sh.
# If it does, then output GFANZ_TEST_RESULTS=OK. If it does not, then output
# GFANZ_TEST_RESULTS=ERROR.
/usr/bin/seqkit | grep Seqkit >>/dev/null

if [ $? -eq 0 ]; then
    echo "GFANZ_TEST_RESULTS=OK"
    exit 0
else
    echo "GFANZ_TEST_RESULTS=ERROR"
    exit 1
fi
