## Tool Name
seqkit
## Version
2.3.1
## Citation
https://doi.org/10.1002/imt2.191
## How to use GFANZ Docker Containers
https://gitlab.com/gfanz/genomics-tools/-/wikis/Using-GFANZ-Genomics-Tools
## Documentation
https://bioinf.shenwei.me/seqkit/
## Source Code
https://github.com/shenwei356/seqkit
## License
MIT
